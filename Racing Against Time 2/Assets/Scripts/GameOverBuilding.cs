using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverBuilding : MonoBehaviour
{
    public TextMeshProUGUI gameOverText; // Reference Game Over Text to make it visible on game over

    public bool isGameOver;

    public int playerCounter;
    
    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
        gameOverText.enabled = false; // Disable game over text
        playerCounter = 0;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            playerCounter ++;
            if(playerCounter >= 1)
            {
                gameOverText.enabled = true; // Make game over text visible    
            Time.timeScale = 0; // Freeze the game
            isGameOver = true;
            Debug.Log("You crashed into a giant building!");
            Destroy(other.gameObject);
            }


        }
        
        Destroy(other.gameObject); // Destroy all game objects that come through the trigger
    }
}
