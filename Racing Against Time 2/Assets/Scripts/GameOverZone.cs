using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverZone : MonoBehaviour
{
    public TextMeshProUGUI gameOverText; // Reference Game Over Text to make it visible on game over

    public bool isGameOver;

    public int enemyCounter;

    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
        gameOverText.enabled = false; // Disable game over text
        enemyCounter = 0;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Enemy"))
        {
            enemyCounter ++;
            if(enemyCounter >= 2)
            {
                gameOverText.enabled = true; // Make game over text visible    
            Time.timeScale = 0; // Freeze the game
            isGameOver = true;
            Debug.Log("The Enemy has made it to the Time Portal before you!");
            Destroy(other.gameObject);
            }


        }
        
        Destroy(other.gameObject); // Destroy all game objects that come through the trigger
    }
}
