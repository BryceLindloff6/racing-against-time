using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    public GameObject[] blimpPrefabs; // This array will hold more than one enemy unit

    private float spawnRangeX = 15.5f;

    private float spawnPosZ = -12.5f;

    private float startDelay = 1.5f;
    private float spawnInterval = 2.0f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnRandomEnemy", startDelay, spawnInterval);
    }

    void SpawnRandomEnemy()
    {
        // Generate a position to spawn at on the x-axis
        Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);
        
        // Pick a random enemy / ufo from the array to spawn
        int enemyIndex = Random.Range(0,blimpPrefabs.Length);
        
        // Spawn the enemy indexed from the array
        Instantiate(blimpPrefabs[enemyIndex], spawnPos, blimpPrefabs[enemyIndex].transform.rotation);
    }

}