using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCollisions : MonoBehaviour
{
    private ScoreManager scoreManager;
    public int scoreToGive;
    public ParticleSystem explosionParticle; // Store the particle system for an explosion

    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject); // Destroy this game object (Player Blimp)
            Destroy(other.gameObject); // Destroy the other game object it hits
            Explosion();
            scoreManager.IncreaseScore(scoreToGive);
        }

    }

    void Explosion()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);
    }
}
