using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float hInput; // Store horizontal Input
    public float vInput; // Store vertical Input
    public float speed; // How fast the player moves
    public float xRange = 14.75f; // Limit left and right movement, Keeps you from leaving the screen
    public float zRange = 5.45f; // Limit forward and back movement

    public GameObject bullet; // Reference to the prefab game object to shoot
    public Transform firePoint; // Point of origin for the bullet

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Set horizontal input to recieve values from keyboard keymap "horizontal"
        hInput = Input.GetAxis("Horizontal");
       
        // Set vertical input to recieve values from keyboard keymap "vertical"
        vInput = Input.GetAxis("Vertical");

        // Move the player left and right
        transform.Translate(Vector3.right * hInput * speed * Time.deltaTime);

        // Move player forward and back
        transform.Translate(Vector3.forward * vInput * speed * Time.deltaTime);

        // Keep player within screen bounds
       
        // Right side
        if(transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }
       
        // Left Side
         if(transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }
       
        // Top Screen
        if(transform.position.z > zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, zRange);
        }
       
        // Bottom Screen
        if(transform.position.z < -zRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -zRange);
        }
            // Shoot bullet
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(bullet, firePoint.transform.position, bullet.transform.rotation); // Creates the bullet game object at the firepoint
        }
        
    }
}
