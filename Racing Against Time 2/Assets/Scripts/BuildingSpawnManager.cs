using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingSpawnManager : MonoBehaviour
{
    public GameObject[] buildingPrefabs; // An array that can hold multiple types of buildings

    private float spawnRangeX = 15.5f;

    private float spawnPosZ = 12.5f;

    private float startDelay = 0.5f;
    private float spawnInterval = 5.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating ("SpawnRandomBuilding", startDelay, spawnInterval);
    }

    // Update is called once per frame
    void SpawnRandomBuilding()
    {
        // Pick a position to spawn building on X-axis
        Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);

        // Pick a random building to actually spawn
        int enemyIndex = Random.Range(0,buildingPrefabs.Length);

        // Spawn the building that was indexed from the array
        Instantiate(buildingPrefabs[enemyIndex], spawnPos, buildingPrefabs[enemyIndex].transform.rotation);
    }
}
