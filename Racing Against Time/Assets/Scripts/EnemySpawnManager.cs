using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    public GameObject[] ufoPrefabs; // This array will hold more than one enemy unit

    private float spawnRangeX = 17.0f;

    private float spawnPosZ = 18.4f;

    private float startDelay = 1.5f;
    private float spawnInterval = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnRandomEnemy", startDelay, spawnInterval);
    }

    void SpawnRandomEnemy()
    {
        // Generate a position to spawn at on the x-axis
        Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);
        // Pick a random enemy / ufo from the array to spawn
        int enemyIndex = Random.Range(0,ufoPrefabs.Length);
        // Spawn the enemy indexed from the array
        Instantiate(ufoPrefabs[enemyIndex], spawnPos, ufoPrefabs[enemyIndex].transform.rotation);
    }

}