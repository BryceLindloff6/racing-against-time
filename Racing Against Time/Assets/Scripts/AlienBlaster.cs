using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienBlaster : MonoBehaviour
{
    public GameObject laserblast;
    public Transform UFO_FirePoint;

    public float startDelay = 3.0f;
    public float spawnInterval = 1.5f;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("AlienShoot", startDelay, spawnInterval);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void AlienShoot()
    {
        Instantiate(laserblast, UFO_FirePoint.transform.position, UFO_FirePoint.transform.rotation);
    }
}
