using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float hInput; // Store horizontal Input
    public float speed; // How fast the player moves
    public float xRange = 14.75f; // Limit left and right movement, Keeps you from leaving the screen

    public GameObject laserblast; // Reference to the prefab game object to shoot
    public Transform firePoint; // Point of origin for the laserblast

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Set horizontal input to recieve values from keyboard keymap "horizontal"
        hInput = Input.GetAxis("Horizontal");

        // Move the player left and right
        transform.Translate(Vector3.right * hInput * speed * Time.deltaTime);

        // Keep player within screen bounds
        // Right side
        if(transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }
        // Left Side
         if(transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }
            // Shoot laserblast
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(laserblast, firePoint.transform.position, laserblast.transform.rotation); // Creates the laserblast game object at the firepoint
        }
        
    }
}
